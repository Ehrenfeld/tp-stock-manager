// Modèle de données produits
var products = [
    
];

// DOMContentLoaded
$(function() {
    
 //===========VARIABLE==============//
    var
        $num_products = 0,

        $card_body = $('.card-body'),
        $new_name = $('#add-name'),
        $new_price = $('#add-price'),
        $new_stock = $('#add-stock'),

        activateFieldError = function( $input, message ) {
            // On vérifie si le champs n'est pas déjà en erreur
            // Pour éviter la multiplication du message
            if( ! $input.hasClass( 'has-error' ) ) {
                $input
                    // On ajoute une class 'border-danger' pour la couleur
                    // On ajoute une class 'has-error' qui office de flag
                    .addClass( 'border-danger has-error' )
                    // On vide le champs
                    .val('')
                    // On insère le message d'erreur à la suite du champs
                    .after( '<small class="form-text text-danger">' + message + '</small>' );
            }};


    products.forEach(function(i) {
    //=========================//
    });



    $card_body.on( 'submit', function( evt ) {
        evt.preventDefault();   


        var
            
            string_stock_val = $new_stock.val(),
            stock_val = parseInt(string_stock_val),

            string_price_val = $new_price.val(),
            price_val = parseInt(string_price_val),

            name_val = $new_name.val(),
            
            $new_products = $('#products'),
            
            stock_stat_add = function() {
       
                if ( stock_val <= 10 ) {
                    $td_stock.addClass('table-warning');
                    $td_stock.removeClass('table-danger');
                }
                else {
                    $td_stock.removeClass('table-warning');
                }
            },

            stock_stat_del = function() { 

                if ( stock_val <= 0 ) {
                    $td_stock.addClass('table-danger');
                }
                else {
                    stock_stat_add();
                }
            },

            stock_stat = function(){
                stock_stat_add();
                stock_stat_del();
            };

                products.push({
                            name: name_val,
                            price: price_val,
                            stock: stock_val
            
                        });
                    

        var
            $new_article = $( 
                `<tr>
                        <td class="w-100"> ${name_val} </td>
                        <td> ${price_val} </td>
                </tr>`),

            $td_stock = $(`<td class="stock"> ${stock_val} </td>`)

            $td_btn_stock = $(`<td class="text-nowrap"> </td>`),     
             
            $td_stock_del = $(`<button type="button" class="btn btn-outline-primary btn-sm stock-del">&minus;</button>`),
             
            $td_stock_add = $(`<button type="button" class="btn btn-primary btn-sm stock-add">&plus;</button>`)
                        
            $del_btn = $(`
            <td>
                <button type="button" class=" btn btn-danger btn-sm product-del">&Cross;</button>
            </td>`);        
                    
            
                
        $new_article.append($td_stock);
        $new_article.append($td_btn_stock);
        $td_btn_stock.append($td_stock_del);
        $td_btn_stock.append($td_stock_add);
        $new_article.append($del_btn);
        $new_products.append($new_article);       

        $num_products++; 

        stock_stat();


        $td_stock_add.on('click', function(){
            
            stock_val = stock_val + 1;

            $td_stock.text(stock_val);
            stock_stat_add();
        });

        $td_stock_del.on('click', function(){
            
            stock_val = stock_val - 1;

            $td_stock.text(stock_val);        
            stock_stat_del();
        });

        

        $del_btn.on( 'click', function() {

            $num_products--; 

            $new_article.remove();


            if ($num_products <= 0) {
                $('.alert').removeClass('d-none');
            }

                     

        });     

        if ($num_products > 0 ){
            $('.alert').addClass('d-none');

        }
        

        if( price_val <= 0 || price_val === '' ) {
        
            activateFieldError( $new_price, 'Veuillez saisir un prix !' );
        
            $new_price.focus();

            $new_price.val('');

            return;
        };  
        
        localStorage.setItem('products', JSON.stringify(products));
    });    
    
    
    var storage_products = JSON.parse( localStorage.getItem( 'products' ) );

    
            if( storage_products !== null ) {
                products = storage_products;
            }
    
    else {
        localStorage.setItem( 'products', JSON.stringify( products ) );
    }

    
            
        
 //////====//////
        $new_price.on( 'keydown', function() {
            var $this = $(this);
            
            if( ! $this.hasClass( 'has-error' ) ) {
                return;
            }

            $this.removeClass( 'has-error border-danger' );

            $this
                .next( 'small' )

               .remove()
        });
});