/* PLAN DE BATAILLE
CSS:
1- bootstrap.css
2- style.css

JS:
1- jQuery slim
2- popper.js
3- bootstrap.js
4- script.js

HTML:
- index.html
- dashboard.html
*/
const
    { src, dest, watch, series, parallel } = require( 'gulp' ),
    concat = require( 'gulp-concat' ),
    minCss = require( 'gulp-clean-css' ),
    minJs = require( 'gulp-babel-minify' ),
    bSync = require( 'browser-sync' );

var
    pathes = {
        build: 'dist',
        html: 'src/',
        css: 'src/css/',
        js: 'src/js/'
    };

// Tâche copie de HTML
function copyHTML() {
    return src( pathes.html + '*.html' )
        .pipe( dest( pathes.build ) );
}

// Tâche construction CSS
function buildCSS() {
    return src( [ 
            'node_modules/bootstrap/dist/css/bootstrap.css',
            pathes.css + 'style.css'
        ] )
        .pipe( concat( 'style.css' ) )
        .pipe( minCss({
            level: {
                1: {
                    specialComments: 'none'
                }
            }
        }) )
        .pipe( dest( pathes.build ) );
}

// Tâche construction JS
function buildJS() {
    return src( [
        'node_modules/jquery/dist/jquery.slim.js',
        'node_modules/popper.js/dist/umd/popper.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        pathes.js + 'script.js'
    ] )
    .pipe( concat( 'script.js' ) )
    .pipe( minJs({}, { comments: false }) )
    .pipe( dest( pathes.build ) );
}

// Tâche rechargement du serveur de développement
function bsReload( taskCallback ) {
    bSync.reload();
    taskCallback();
}

// Tâche d'initialisation du serveur de développement
function serve( taskCallback ) {
    bSync.init({
        server: {
            baseDir: pathes.build
        }
    });

    // Surveillance HTML
    watch( pathes.html + '*.html', series( copyHTML, bsReload ) );

    // Surveillance CSS
    watch( pathes.css + '**/*.css', series( buildCSS, bsReload ) );

    // Surveillance JS
    watch( pathes.js + '**/*.js', series( buildJS, bsReload ) );

    taskCallback();
}


// Exports des tâches individuelles
exports.copyHTML = copyHTML;
exports.buildCSS = buildCSS;
exports.buildJS = buildJS;
exports.serve = serve;

// Export de la tâche par défaut
exports.default = series( parallel( copyHTML, buildCSS, buildJS ), serve );
